<?php 

/**
*  Corresponding test class to test Maths class
*  @author Alex Paphitis
*/

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Pleiades\Maths\Maths;

class MathsTest extends TestCase {
	/**
	* Test Maths class to find next greater number with same digits
	*/
	public function testNextGreaterNumber() {
		$this->assertSame(Maths::nextGreaterNumber(534976), 536479);
	}

	/**
	* Test Maths class to find missing positive number from array
	*/
	public function testFindMissingPositive() {
		$this->assertSame(Maths::findMissingPositive([2,3,-7,6,8,1,-10,15]), 4);
	}

	/**
	* Test Maths class to find minimum number out of two
	*/
	public function testMinimum() {
		$this->assertSame(Maths::min(5, 10), 5);
	}

	/**
	* Test Maths class to find minimum number out of two
	*/
	public function testMaximum() {
		$this->assertSame(Maths::max(5, 10), 10);
	}

	/**
	* Test Maths class to find average of array of numbers
	*/
	public function testAverage() {
		$this->assertSame(Maths::avg([1,2,3]), 2);
	}
}