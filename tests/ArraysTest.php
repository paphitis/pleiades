<?php 

/**
*  Corresponding test class to test Array class
*  @author Alex Paphitis
*/

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Pleiades\Arrays\Arrays;

class ArrayTest extends TestCase {
	/**
	* Test Arrays class to get false values removed from array
	*/
	public function testCompact() {
		$this->assertTrue(Arrays::compact([0, true, "", -1, "test"]) == [true, -1, "test"]);
	}

	/**
	* Test Arrays class to find number of occurences of a value in an array
	*/
	public function testFindOccurences() {
		$array = ["test", "test", "test", 3, 4, "test"];
		$this->assertTrue(Arrays::findOccurences($array, "test") == 4);
	}

	/**
	* Test Arrays class to flatten an array
	*/
	public function testFlatten() {
		$array = ["test", "test", ["test", 3, ["test"]], 4, "test"];
		$expected = ["test", "test", "test", 3, "test", 4, "test"];

		$this->assertTrue(Arrays::flatten($array) == $expected);
	}
}