<?php 

/**
*  Corresponding test class to test Strings class
*  @author Alex Paphitis
*/

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Pleiades\Strings\Strings;

class StringsTest extends TestCase {
	/**
	* Test Strings class for syntax errors
	*/
	public function testIsThereAnySyntaxError() {
		$var = new Strings("test");
		$this->assertTrue(is_object($var));
		unset($var);
	}

	/**
	* Test Strings instance returns string stored
	*/
	public function testInstanceReturnsString() {
		$var = new Strings("test");
		$this->assertTrue($var == 'test');
		unset($var);
	}

	/**
	* Test append method
	*/
	public function testAppend() {
		$var = new Strings("test");
		$newString = $var->append(" test");
		$this->assertTrue($newString == 'test test');
		unset($var);
	}

	/**
	* Test at method returned character at index
	*/
	public function testAt() {
		$var = new Strings("test");
		$this->assertTrue($var->at(1) == 'e');
		unset($var);
	}

	/**
	* Test lowercase method returns lowercase version of string
	*/
	public function testLowercase() {
		$var = new Strings("TEST");
		$this->assertTrue($var->lowercase() == 'test');
		unset($var);
	}

	/**
	* Test uppercase method returns uppercase version of string
	*/
	public function testUppercase() {
		$var = new Strings("test");
		$this->assertTrue($var->uppercase() == 'TEST');
		unset($var);
	}

	/**
	* Test toArray method returns array with each character in the string as an element
	*/
	public function testToArray() {
		$var = new Strings("test");
		$this->assertTrue($var->toArray() == ['t', 'e', 's', 't']);
		unset($var);
	}

	/**
	* Test truncate method truncates string to given length
	*/
	public function testTruncate() {
		$var = new Strings("testing");
		$this->assertTrue($var->truncate(4) == "test");
		unset($var);
	}

	/**
	* Test tabify method converts spaces to tabs
	*/
	public function testTabify() {
		$var = new Strings("    test");
		$this->assertTrue($var->tabify() == "	test");
		unset($var);
	}


	/**
	* Test spacify method converts tabs to spaces
	*/
	public function testSpacify() {
		$var = new Strings("	test");
		$this->assertTrue($var->spacify() == "    test");
		unset($var);
	}
}