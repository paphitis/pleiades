<?php namespace Pleiades\Arrays;

/**
*  Arrays
*
*  Various array utility functions
*
*  @author Alex Paphitis
*/
class Arrays {
	/**
	 * Remove the false values from the given array
	 *
	 * @param  array  $array Array to remove false values from
	 * @return array The array with false values removed
	*/
	public static function compact($array) {
		$newArray = [];

		foreach ($array as $key => $el)
			if ($el) array_push($newArray, $array[$key]);

		return $newArray;
	}

	/**
	 * Count the occurences of a value in an array
	 *
	 * @param  array  $array Array to search
	 * @param mixed $value The value to search for
	 * @return array The number of occurences of the value in the array
	*/
	public static function findOccurences($array, $value) {
		$count = 0;

		for ($i = 0; $i < count($array); $i++) { 
			if ($value == $array[$i])
				$count += 1;
		}

		return $count;
	}

	/**
	* Flatten a multi dimensional array into a one dimensional array
	*
	* @param  array   $array The array to flatten
	* @param  boolean $preserveKeys Whether or not to keep the keys in the array
	* @return array
	*/
	public static function flatten($array, $preserveKeys = true) {
		$flattened = [];

		foreach ($array as $key => $value) {
			// If we're preserving the key add it to the flattened array
			if ($preserveKeys && !is_int($key)) 
				array_push($flattened, $key);

			// If it's not an array inside then add it to the list and go to the next element
			if (!is_array($value)) {
				array_push($flattened, $value);
				continue;
			}

			// Recursively flatten the array, add each element returned to the flattened array
			$innerArray = Arrays::flatten($value, $preserveKeys);

			foreach ($innerArray as $innerValue) {
				array_push($flattened, $innerValue);
			}			
		}

		return $flattened;
	}
}