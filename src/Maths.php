<?php namespace Pleiades\Maths;

/**
*  Maths
*
*  Various math utility functions
*
*  @author Alex Paphitis
*/
class Maths {
	/**
	 * Return next greater number that has the same digits
	 *
	 * @param  integer  $number Number to use
	 * @return integer Next greatest number that has same digits
	*/
	public static function nextGreaterNumber($number) {
		$numberString = (string) $number;
		$numberArray = [];
		$numberLength = strlen($numberString);

		// Convert number to array of integers
		for ($i = 0; $i < $numberLength; $i++)
			array_push($numberArray, (int) $numberString[$i]);

		// If the number is already in descending order, the next greatest number
		// does not exist
		for ($i = $numberLength - 1; $i > 0; $i--) 
			if ($numberArray[$i] > $numberArray[$i - 1]) 
				break; 

		// Return if it doesn't exist
		if ($i == 0) { 
			return -1; 
		}

		$x = $numberArray[$i - 1];
		$smallest = $i;

		for ($j = $i + 1; $j < $numberLength; $j++) 
			if ($numberArray[$j] > $x && $numberArray[$j] < $numberArray[$smallest]) 
				$smallest = $j;

		// Swap the numbers around
		$temp = $numberArray[$smallest];
		$numberArray[$smallest] = $numberArray[$i - 1];
		$numberArray[$i - 1] = $temp;

		// Get the last number of elements
		// Sort them in ascending order
		// Merge them back into the array, replacing the original elements at the end
		$subset = array_slice($numberArray, $i);
		sort($subset);
		array_splice($numberArray, $i, count($subset), $subset);

		// Convert back to a number
		// Concat to a string, add each element to the string
		// then cast to an integer
		$nextNumberString = "";

		for ($i = 0; $i < $numberLength; $i++)
			$nextNumberString .= $numberArray[$i];

		$nextNumber = (int) $nextNumberString;
		return $nextNumber;
	}

	/**
	 * Find smallest missing positive number from array
	 *
	 * @param  integer  $number Array to search
	 * @return integer Smallest missing positive number from array
	*/
	public static function findMissingPositive($array) {
		$smallest = 1;
		$arrayLength = count($array);
		$x = []; // Stores values that are greater than the smallest value

		for ($i = 0; $i < $arrayLength; $i++) {
			// Store element if it's greater than smallest value
			if ($smallest < $array[$i])
				array_push($x, $array[$i]);

			if ($smallest != $array[$i])
				continue;

			// Increment by 1 if the element is equal to the smallest value
			// This way we're working on the next highest number when we loop
			$smallest += 1;

			// Loop through the array that stores numbers greater than
			// the current smallest value.
			// We keep looping as long as we can find occurences of
			// the smallest value in the array
			while (array_key_exists($smallest, array_count_values($x))) {
				array_splice($x, array_search($smallest, $x), 1); // Remove value if it's in the array
				$smallest += 1;
			}
		}

		return $smallest;
	}

    /**
	 * Return minimum between two numbers
	 *
	 * @param  integer  $a First number
	 * @param integer $b Second number
	 * @return integer Smallest number out of the two
	*/
	public static function min($a, $b) {
		if ($a < $b) return $a;
		return $b;
	}

    /**
	 * Return maximum between two numbers
	 *
	 * @param  integer  $a First number
	 * @param integer $b Second number
	 * @return integer Smallest number out of the two
	*/
	public static function max($a, $b) {
		if ($a > $b) return $a;
		return $b;
	}

    /**
	 * Return average of array of numbers
	 *
	 * @param  array  $array Array of numbers
	 * @return integer Average of the array of numbers
	*/
	public static function avg($array) {
		$sum = array_sum($array);
		return $sum / count($array);
	}
}