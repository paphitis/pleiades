<?php namespace Pleiades\Strings;

/**
*  Strings
*
*  Various string utility functions
*
*  @author Alex Paphitis
*/
class Strings {
	/**
	 * The instances string
	 * @var string
	*/
	protected $str;

	/**
	 * The string's encoding
	 * @var string
	*/
	protected $encoding;

	/**
	 * Initialize a Strings object and assigns both str and encoding properties
	 * the supplied values
	 * @param  mixed $str String to store
	 * @param  string $encoding The encoding of the string
	*/
	public function __construct($str = '', $encoding = null) {
		if ($encoding == null) $encoding = "utf-8";

		$this->str = (string) $str;
		$this->encoding = $encoding;
	}

	/**
	* Returns the value of the string
	* @return string The value of the $str property
	*/
	public function __toString() {
		return $this->str;
	}

	/**
	 * Create a Strings object using passed string and encoding
	 *
	 * @param  mixed  $str String to store
	 * @param  string $encoding String character encoding
	 * @return static A Strings object
	*/
	public static function create($str = '', $encoding = null) {
		if ($encoding == null) $encoding = "utf-8";
		return new static($str, $encoding);
	}

	/**
	 * Append $string to the stored string
	 * @param  string $string The string to append
	 * @return static Object with appended $string
	*/
	public function append($string) {
		return static::create($this->str . $string, $this->encoding);
	}

	/**
	 * Return the character at $index
	 * @param  int    $index Position of the character
	 * @return static The character at $index
	*/
	public function at($index) {
		$char = substr($this->str, $index, 1);
		return static::create($char, $this->encoding);
	}

	/**
	 * Return the lowercase version of the string stored
	 * @return static The lowercase version of the string
	*/
	public function lowercase() {
		$str = \mb_strtolower($this->str, $this->encoding);
		return static::create($str, $this->encoding);
	}

	/**
	 * Return the uppercase version of the string stored
	 * @return static The uppercase version of the string
	*/
	public function uppercase() {
		$str = \mb_strtoupper($this->str, $this->encoding);
		return static::create($str, $this->encoding);
	}

	/**
	 * Return array with each character in the stored string as an element
	 * @return array Array made up of each character in the string
	*/
	public function toArray() {
		$array = [];
		$str = $this->str;
		$len = strlen($this->str);

		for ($i = 0; $i < $len; $i++)
			array_push($array, $str[$i]);

		return $array;
	}

	/**
	 * Return version of string truncated to given length
	 * @param int The length to truncate to
	 * @return static Version of string truncated to length given
	*/
	public function truncate($length) {
		$truncated = "";
		$str = $this->str;
		$len = strlen($this->str);

		for ($i = 0; $i < $len && $i < $length; $i++)
			$truncated .= $str[$i];

		return static::create($truncated, $this->encoding);
	}

	/**
	 * Return version of string with consecutive number of spaces converted to tabs
	 * @param int The number of consecutive spaces to consider as a tab
	 * @return static Version of string with consecutive number of spaces converted to tabs
	*/
	public function tabify($length = 4) {
		$spaces = str_repeat(' ', $length);
		$str = str_replace($spaces, "\t", $this->str);
		return static::create($str, $this->encoding);
	}

	/**
	 * Return version of string with tabs converted to consecutive number of spaces
	 * @param int The number of consecutive spaces to replace a tab with
	 * @return static Version of string with tabs converted to consecutive number of spaces
	*/
	public function spacify($length = 4) {
		$spaces = str_repeat(' ', $length);
		$str = str_replace("\t", $spaces, $this->str);
		return static::create($str, $this->encoding);
	}
}